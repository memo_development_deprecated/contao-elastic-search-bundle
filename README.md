# Memo Elastic Search Bundle

## About

Dieses Bundle ermöglicht Ihnen eine Anbindung zu einem Elastic Search Server und somit eine erweiterte Seitensuche. Diverse Funktionen von Elastic Search können mit diesem Bundle verwendet werden (Suchvorschläge, Synonyme, Ungenaue Suche...).

## Installation

Installiere [composer](https://getcomposer.org) falls dieser nicht bereits installiert ist.
Füge das ungelistete Repo (nicht auf packagist.org) dem composer.json: hinzu

```
"repositories": [
  {
    "type": "vcs",
    "url" : "git@bitbucket.org:memo_development/contao-elastic-search-bundle.git"
  }
],
```

Füge das Bundle den requirements hinzu:
```
"memo_development/contao-elastic-search-bundle": "^0.0",
```

## Usage
### 1. Bundle installieren
### 2. Die Contao Suche muss deaktivieren werden, fügen Sie folgenden Code ins config.yml ein:

```
# config/config.yaml
contao:
    search:
        default_indexer:
            enable: false
        listener:
            index: true # Configure whether you want to update the index entry on every request
            delete: false # Configure whether you want to delete an entry if a request is not successful
```

### 3. Füge pro Domäne / Sprache einen Index (inkl. Serverangaben) hinzu

```
# config/config.yaml
memo_elastic_search:
    connections:
        -
            url_match: '/domain\.ch\/de\//'
            scheme: 'https'
            host: 'hostname.ch'
            port: 443
            user: 'your_username'
            password: 'your_password'
            search_result_tags: '<br>,<p>,<strong>,<b>,<em>,<ul>,<ol>,<li>'
            index: 'co_domain_de'
            default_language: 'de'
            label: 'Custom Label'
        -
            url_match: '/domain\.ch\/fr\//'
            scheme: 'https'
            host: 'hostname.ch'
            port: 443
            user: 'your_username'
            password: 'your_password'
            search_result_tags: '<br>,<p>,<strong>,<b>,<em>,<ul>,<ol>,<li>'
            index: 'co_domain_fr'
            default_language: 'fr'
            label: 'Custom Label'
        -
            ...
```

#### Parameters
| Key | Description |
| --------- | ------------ |
| url_match | Regular Expression für die Erkennung der zu indexierenden URL's pro Konfiguration |
| scheme | Elastic Server Sheme |
| host | Elastic Server Host |
| port | Elastic Server Port |
| user | Elastic Server User |
| password | Elastic Server Password |
| search_result_tags | Tags welche im Suchresultat angezeigt werden sollen. |
| priorities | Suchrelevanz der Inhalte |
| index | Frei definierbarer Name des Indexes (wird automatisch erstellt) |
| default_language | Standard Sprache |
| label | Frei definierbarer Label für die Auswahl im Backend |


### 4. Konsole/Systemwartung "XML-Dateien neu schreiben" neu generieren
### 5. Leere und  generiere den Suchindex über die Konsole/Systemwartung neu (Index wird anhand der Sitemap.xml generiert)
### 6. Erstelle ein FrontendModul «Erweitertes Suchformular»
### 7. Binde das FrontendModul in einer Seite ein.


> **Hinweis: Im Seitenbaum muss XML-Sitemap aktiv und aktuell sein die Domain der Webseite hinterlegt sein. Ansonsten können keine Daten indexiert werden.**



## Optional

Damit der Crawler regelmässig die aktuellen Inhalte in den Suchindex aufnimmt, richten Sie ein Cronjob ein:
```
/usr/local/bin/php74 path_to_your_contao_installation/vendor/bin/contao-console contao:automator purgeSearchTables
/usr/local/bin/php74 path_to_your_contao_installation/vendor/bin/contao-console contao:crawl --subscribers=search-index --concurrency=5
```

## Search AJAX API
@Route("/ajax_elasticsearch")
### Get Parameter
|Parameter |Type | Description |
|----------|-----|-------------|
|keyword | string | Search Phrase |
|api | string | search Index |

## Hooks

### @Hook("indexPageToElasticSearch")

Dieser Hook wird ausgeführt, bevor eine neue Seite in den Elastic Search Index aufgenommen wird. Dies geschieht, wenn der Contao Suchcrawler ausgeführt wird.

**Parameters**
1. *array* $arrParams
Parameter, welche bei Elastic Search indexiert werden
2. *object* $objDocument
Dokument, welches wir vom Contao Crawler erhalten
3. *integer* $intApiKey
ID des Datensatzes mit API, welche verwendet wurde

**Return Values**
$arrParams

### @Hook("indexPdfToElasticSearch")

Dieser Hook wird ausgeführt, bevor ein neues PDF in den Elastic Search Index aufgenommen wird. Dies geschieht, wenn der Contao Suchcrawler ausgeführt wird.

**Parameters**
1. *array* $arrParams
Parameter, welche bei Elastic Search indexiert werden
2. *string* $strPdfLink
Link zum PDF, welches indexiert wird
3. *array* $arrPdfDetails
Details zum PDF, welche der PDF Parser auslesen kann
4. *integer* $intApiKey
ID des Datensatzes mit API, welche verwendet wurde
5. *object* $objDocument
Dokument, auf welchem das PDF verlinkt wurde

**Return Values**
$arrParams

### @Hook("deleteIndexFromElasticSearch")
Dieser Hook wird ausgeführt, nachdem ein kompletter Index gelöscht wurde.

**Parameters**
1. *array* $arrResponse
Antwort, welche ElasticSearch nach dem löschen des Indexes liefert
2. *array* $arrParams
Parameter, welche zum löschen des Indexes verwendet wurden
3. *object* $objApi
API, welche zum löschen des Indexes verwendet wurde

**Return Values**
Kein Return Value

## Requirements

* PHP 7.4+
* Contao 4.9+
* elasticsearch/elasticsearch
* menatwork/contao-multicolumnwizard-bundle
* codefog/contao-haste
* smalot/pdfparser

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
