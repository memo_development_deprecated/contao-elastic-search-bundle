<?php


namespace Memo\ElasticSearchBundle\Model;


class ElasticSearchFieldsRelationModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 **/
	protected static $strTable = 'tl_elasticsearch_fields_relation';
}
