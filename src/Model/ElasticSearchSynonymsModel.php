<?php

namespace Memo\ElasticSearchBundle\Model;

use Elasticsearch\ClientBuilder;


class ElasticSearchSynonymsModel extends \Model
{

	/**
     * Table name
     * @var string
    **/
    protected static $strTable = 'tl_elasticsearch_synonyms';
}
