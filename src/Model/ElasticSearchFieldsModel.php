<?php


namespace Memo\ElasticSearchBundle\Model;


class ElasticSearchFieldsModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 **/
	protected static $strTable = 'tl_elasticsearch_fields';
}
