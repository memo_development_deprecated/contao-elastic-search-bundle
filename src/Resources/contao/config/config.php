<?php declare(strict_types=1);

/**
 * @package   Memo\MemoElasticSearchBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
array_insert($GLOBALS['BE_MOD']['memo_elasticsearch'], 100, array
(
	'synonyms' => array
	(
		'tables'       => array(
			'tl_elasticsearch_synonyms',
		),
	),
	'search_fields' => array
	(
		'tables'	=> array(
			'tl_elasticsearch_fields',
			'tl_elasticsearch_fields_relation'
		)
	)
));

/**
 * Add front end modules and Custom Elements
 */

$GLOBALS['FE_MOD']['elasticSearch']['elasticSearchForm'] = 'Memo\ElasticSearchBundle\Module\ElasticSearchFormModule';

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_elasticsearch_synonyms']   = 'Memo\ElasticSearchBundle\Model\ElasticSearchSynonymsModel';
$GLOBALS['TL_MODELS']['tl_elasticsearch_fields']   = 'Memo\ElasticSearchBundle\Model\ElasticSearchFieldsModel';
$GLOBALS['TL_MODELS']['tl_elasticsearch_fields_relation']   = 'Memo\ElasticSearchBundle\Model\ElasticSearchFieldsRelationModel';


/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
	$GLOBALS['TL_CSS'][]        = '/bundles/memoelasticsearch/backend.css';
}
