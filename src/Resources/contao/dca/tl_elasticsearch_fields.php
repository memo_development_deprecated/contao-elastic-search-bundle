<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_elasticsearch_fields
 */

use Memo\ElasticSearchBundle\Service\ElasticSearchService;

$GLOBALS['TL_DCA']['tl_elasticsearch_fields'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'switchToEdit'				  => true,
		'ctable'                      => array('tl_elasticsearch_fields_relation'),
		'markAsCopy'                  => 'name',
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 0,
			'panelLayout'             => 'filter;sort,search,limit',
			'fields'				  => ['name']
		),
		'label' => array
		(
			'fields'                  => array('name','elasticSearchApi'),
			'showColumns'			  => true,
			'format'				  => '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['edit'],
				'href'                => 'table=tl_elasticsearch_fields_relation',
				'icon'                => 'edit.gif'
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.svg',
				'button_callback'     => array('tl_elasticsearch_fields', 'editHeader')
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'				  => [],
		'default'                     => '{fields_legend},name,elasticSearchApi,sfieldWizzard;',
	),

	// Subpalettes
	'subpalettes' => array
	(

	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'name' => array
		(
			'label'               	  => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['name'],
			'exclude'             	  => true,
			'filter'              	  => false,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'           	  => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength' => 255, 'tl_class' => 'clr'),
			'sql'                 	  => "varchar(255) NOT NULL default ''",
		),


		'elasticSearchApi' => [
			'label'            => &$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['elasticSearchApi'],
			'exclude'          => true,
			'inputType'        => 'select',
			'options_callback' 	=> array('tl_elasticsearch_fields', 'getApis'),
			'eval'             => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50 clr', 'includeBlankOption' => true, 'submitOnChange'=>true),
			'sql'              => "varchar(255) NULL",
		]
	)
);

/**
 * Class tl_elasticsearch_fields
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_elasticsearch_fields extends \Contao\Backend
{
	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('Contao\BackendUser', 'User');

	}

	/**
	 * @param $dc
	 * @return array
	 */
	public function getApis($dc) {

		$arrOptions = array();
		if($aConnections = \System::getContainer()->getParameter('memo_elastic_search.connections')){
			foreach($aConnections as $aConnection){
				$strIndex = $aConnection['index'];
				$strLabel = $aConnection['label'];

				$arrOptions[$strIndex] = ($strLabel != '') ? $strLabel : $strIndex;
			}
		}

		return $arrOptions;
	}


	/**
	 * Return the edit header button
	 *
	 * @param array  $row
	 * @param string $href
	 * @param string $label
	 * @param string $title
	 * @param string $icon
	 * @param string $attributes
	 *
	 * @return string
	 */
	public function editHeader($row, $href, $label, $title, $icon, $attributes)
	{
		return $this->User->canEditFieldsOf('tl_elasticsearch_fields_relation') ? '<a href="' . $this->addToUrl($href . '&amp;id=' . $row['id']) . '" title="' . Contao\StringUtil::specialchars($title) . '"' . $attributes . '>' . Contao\Image::getHtml($icon, $label) . '</a> ' : Contao\Image::getHtml(preg_replace('/\.svg$/i', '_.svg', $icon)) . ' ';
	}



}
