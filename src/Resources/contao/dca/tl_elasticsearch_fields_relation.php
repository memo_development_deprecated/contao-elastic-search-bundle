<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_elasticsearch_fields_relation
 */
$GLOBALS['TL_DCA']['tl_elasticsearch_fields_relation'] = array
(

	'config' => array
	(
		'dataContainer'               => 'Table',
		'enableVersioning'            => true,
		'ptable'                      => 'tl_elasticsearch_fields',
		'switchToEdit'                => true,
		'markAsCopy'                  => 'title',
		'onload_callback' => array
		(

		),
		'oncreate_callback' => array
		(
		),
		'oncopy_callback' => array
		(
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid' => 'index'
			)
		)
	),
	// List
	'list' => array
	(

		'sorting' => array
		(
			'mode'                    => 0,
			'fields'                  => array(),
			'flag'                    => 1,
			'panelLayout'             => 'filter;sort,search,limit',
		),
		'label' => array
		(
			'fields'                  => array('title','search_fields','weight'),
			'showColumns'			  => true,
			'format'                  => '%s'
		),

		'global_operations' => array
		(
			'all' => array
			(
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),

		'operations' => array
		(
			'edit' => array
			(
				'href'                => 'act=edit',
				'icon'                => 'edit.svg'
			),
			'copy' => array
			(
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			),
		),
	),
	'palettes' => array
	(
		'default'                     => '{fields_legend},title,search_fields,weight'
	),
	// Subpalettes
	'subpalettes' => array
	(
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'              => 'tl_elasticsearch_fields_relation.id',
			'sql'                     => "int(10) unsigned NOT NULL default 0",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default 0"
		),
		'title' => array
		(
			'exclude'                 => false,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'clr'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),
		'search_fields' => array
		(
			'exclude'             	  => true,
			'filter'              	  => true,
			'search'                  => true,
			'sorting'                 => false,
			'inputType'           	  => 'select',
			'options_callback' 		  => array('tl_elasticsearch_fields_relation', 'getSearchFields'),
			'eval'                    => array('mandatory'=>false, 'multiple' => false,'includeBlankOption'=>true,'blankOptionLabel'=>'-- Bitte wählen --', 'tl_class' => 'w50','chosen'=>true),
			'sql'                     => "blob NULL",
		),
		'weight' => array
		(
			'exclude'                 => false,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'tl_class'=>'w50'),
			'sql'                     => "varchar(255) NOT NULL default ''"
		),

	)
);


use Memo\ElasticSearchBundle\Model\ElasticSearchFieldsModel;
use Memo\ElasticSearchBundle\Service\ElasticSearchService;

class tl_elasticsearch_fields_relation extends Contao\Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('Contao\BackendUser', 'User');

	}


	/**
	 * @param DataContainer $dc
	 * @return array with fields
	 */
	public function getSearchFields(DataContainer $dc): array
	{

		if(empty($dc->activeRecord->pid))
		{
			return [];
		}

		$aParent = ElasticSearchFieldsModel::findByPk($dc->activeRecord->pid);
		if(empty($aParent)) {
			return [];
		}
		$objService = ElasticSearchService::getServiceByIndex($aParent->elasticSearchApi);
		if(empty($objService)) {
			return [];
		}

		$arrFields  = $objService->getSearchFields();
		return $arrFields;
	}



}
