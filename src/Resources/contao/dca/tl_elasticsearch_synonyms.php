<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author    Damian Huwiler, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Table tl_elasticsearch_synonyms
 */
$GLOBALS['TL_DCA']['tl_elasticsearch_synonyms'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'switchToEdit'				  => true,
		'enableVersioning'            => true,
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 0,
			'panelLayout'             => 'filter;sort,search,limit',
		),
		'label' => array
		(
			'fields'                  => array('value','elasticSearchApi'),
			'showColumns'			  => true,
			'format'				  => '%s'
		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			),
		)
	),

	// Select
	'select' => array
	(
		'buttons_callback' => array()
	),

	// Edit
	'edit' => array
	(
		'buttons_callback' => array()
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{synonym_legend},value,elasticSearchApi;',
	),

	// Subpalettes
	'subpalettes' => array
	(
		''                            => '',
	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'value' => array
		(
			'label'               	  => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['value'],
			'exclude'             	  => true,
			'filter'              	  => false,
			'search'                  => true,
			'sorting'                 => true,
			'inputType'           	  => 'text',
			'eval'                    => array('mandatory'=>true, 'maxlength' => 255, 'tl_class' => 'clr'),
			'sql'                 	  => "varchar(255) NOT NULL default ''",
		),
		'elasticSearchApi' => [
			'label'            => &$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['elasticSearchApi'],
			'exclude'          => true,
			'filter'          => true,
			'inputType'        => 'select',
			'options_callback' 	=> array('tl_elasticsearch_synonyms', 'getApis'),
			'eval'             => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50 clr', 'includeBlankOption' => true, 'submitOnChange'=>true),
			'sql'              => "varchar(255) NULL",
		]
	)
);

/**
 * Class tl_elasticsearch_synonyms
 * Definition der Callback-Funktionen für das Datengefäss.
 */
class tl_elasticsearch_synonyms extends Backend
{
	public function getApis($dc) {

		$arrOptions = array();
		if($aConnections = \System::getContainer()->getParameter('memo_elastic_search.connections')){
			foreach($aConnections as $aConnection){
				$strIndex = $aConnection['index'];
				$strLabel = $aConnection['label'];

				$arrOptions[$strIndex] = ($strLabel != '') ? $strLabel : $strIndex;
			}
		}

		return $arrOptions;
	}
}
