<?php
/**
 * @package   MailJetBundle
 * @author    Rory Zünd, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Add palettes
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'fuzzy';


$GLOBALS['TL_DCA']['tl_module']['palettes']['elasticSearchForm'] = '{title_legend},name,headline,type;{source_legend},elasticSearchApi,searchFields;{search_legend},elasticSearchOperator,autocomplete,fuzzy,pagination,limit,redirect,limit_chars,no_default_styles;{template_legend:hide},customTpl,searchResultTemplate,searchFormTemplate,paginationTemplate;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['fuzzy'] = 'fuzziness';


/**
 * Add fields
 */

$GLOBALS['TL_DCA']['tl_module']['fields']['elasticSearchApi'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['elasticSearchApi'],
    'exclude'          => true,
    'inputType'        => 'select',
	'options_callback' 	=> array('tl_module_elasticsearch', 'getApis'),
    'eval'             => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange'=>true),
	'sql'              => "varchar(255) NULL",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['elasticSearchOperator'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['elasticSearchOperator'],
    'exclude'          => true,
    'inputType'        => 'select',
    'options' 	       => ['and'=>'Und','or'=>'Oder'],
    'eval'             => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange'=>true, 'default'=>'and'),
    'sql'              => "varchar(255) NULL",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['searchFields'] = [
	'exclude'          => true,
	'inputType'        => 'select',
	'foreignKey' 	   => 'tl_elasticsearch_fields.name',
	'eval'             => array('mandatory' => true, 'multiple' => false, 'tl_class' => 'w50', 'includeBlankOption' => true, 'submitOnChange'=>true),
	'sql'              => "varchar(255) NULL",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['autocomplete'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['autocomplete'],
    'exclude'          => true,
    'inputType'        => 'checkbox',
    'eval'             => array('tl_class' => 'w50 clr'),
    'sql'              => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['fuzzy'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['fuzzy'],
    'exclude'          => true,
    'inputType'        => 'checkbox',
    'eval'             => array('tl_class' => 'w50 clr', 'submitOnChange'=>true),
    'sql'              => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['no_default_styles'] = [
	'exclude'          => true,
	'inputType'        => 'checkbox',
	'eval'             => array('tl_class' => 'w50', 'submitOnChange'=>false),
	'sql'              => "char(1) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['no_default_scripts'] = [
	'exclude'          => true,
	'inputType'        => 'checkbox',
	'eval'             => array('tl_class' => 'w50 clr', 'submitOnChange'=>false),
	'sql'              => "char(1) NOT NULL default ''",
];


$GLOBALS['TL_DCA']['tl_module']['fields']['fuzziness'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['fuzziness'],
    'exclude'          => true,
    'inputType'        => 'text',
    'default'		   => 'auto',
    'eval'             => array('tl_class' => 'w50', 'mandatory'=>true, 'maxlength'=>4),
    'sql'              => "varchar(4) NOT NULL default 'auto'",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['pagination'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['pagination'],
    'exclude'          => true,
    'inputType'        => 'checkbox',
    'eval'             => array('tl_class' => 'w50 clr'),
    'sql'              => "char(1) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['limit'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['limit'],
    'exclude'          => true,
    'inputType'        => 'text',
    'default'		   => 20,
    'eval'             => array('tl_class' => 'w50 clr', 'minval' => 0, 'maxval' => 99, 'mandatory' => true, 'rgxp'=>'natural'),
    'sql'              => "char(2) NOT NULL default '20'",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['redirect'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['redirect'],
    'exclude'          => true,
    'inputType'        => 'pageTree',
	'foreignKey'       => 'tl_page.title',
	'relation'         => array('type'=>'hasOne', 'load'=>'lazy'),
    'eval'             => array('tl_class' => 'clr', 'feildType'=>'radio'),
    'sql'              => "int(10) unsigned NOT NULL default 0",
];



$GLOBALS['TL_DCA']['tl_module']['fields']['limit_chars'] = [
    'label'            => &$GLOBALS['TL_LANG']['tl_module']['limit_chars'],
    'exclude'          => true,
    'inputType'        => 'text',
    'default'		   => 50,
    'eval'             => array('tl_class' => 'w50','maxval' => 9999, 'mandatory' => true, 'rgxp'=>'natural'),
    'sql'              => "char(4) NOT NULL default '50'",
];

$GLOBALS['TL_DCA']['tl_module']['fields']['searchFormTemplate'] = $GLOBALS['TL_DCA']['tl_module']['fields']['customTpl'];
$GLOBALS['TL_DCA']['tl_module']['fields']['searchFormTemplate']['options_callback'] = array('tl_module_elasticsearch', 'getTemplates');
$GLOBALS['TL_DCA']['tl_module']['fields']['searchFormTemplate']['default'] = 'elasticsearch_form';

$GLOBALS['TL_DCA']['tl_module']['fields']['searchResultTemplate'] = $GLOBALS['TL_DCA']['tl_module']['fields']['customTpl'];
$GLOBALS['TL_DCA']['tl_module']['fields']['searchResultTemplate']['options_callback'] = array('tl_module_elasticsearch', 'getTemplates');
$GLOBALS['TL_DCA']['tl_module']['fields']['searchResultTemplate']['default'] = 'elasticsearch_result_item';

$GLOBALS['TL_DCA']['tl_module']['fields']['paginationTemplate'] = $GLOBALS['TL_DCA']['tl_module']['fields']['customTpl'];
$GLOBALS['TL_DCA']['tl_module']['fields']['paginationTemplate']['options_callback'] = array('tl_module_elasticsearch', 'getTemplates');
$GLOBALS['TL_DCA']['tl_module']['fields']['paginationTemplate']['default'] = 'elasticsearch_pagination';

/**
 * Class tl_module_elasticsearch
 */
use Memo\ElasticSearchBundle\Model\ElasticSearchIntegrationModel;
use Memo\ElasticSearchBundle\Service\ElasticSearchService;

class tl_module_elasticsearch extends Backend {

	public function getTemplates($dc) {
		return $this->getTemplateGroup('elasticsearch_');
	}

	public function getElasticSearchFields($dc) {
		if($dc->activeRecord->elasticSearchApi){

			$objService = ElasticSearchService::getServiceByIndex($dc->activeRecord->elasticSearchApi);

			if(!empty($objService)){
				return $objService->getSearchFields();
			}

		}
	}

	/**
	 * @param $dc
	 * @return array
	 */
	public function getApis($dc) {

		$arrOptions = array();
		if($aConnections = \System::getContainer()->getParameter('memo_elastic_search.connections')){
			foreach($aConnections as $aConnection){
				$strIndex = $aConnection['index'];
				$strLabel = $aConnection['label'];
				$arrOptions[$strIndex] = ($strLabel != '') ? $strLabel : $strIndex;
			}
		}

		return $arrOptions;
	}

}
