<?php


$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['fields_legend']		= 'Suchfeld Definition';
$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['name']				= array('Name', '');
$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['elasticSearchApi']	= array('API Konfiguration', 'Welche API sollen verwendet werden?');
$GLOBALS['TL_LANG']['tl_elasticsearch_fields']['sfieldWizzard']		= array('Feld Konfiguration', 'Welche Felder sollen verwendet werden?');
