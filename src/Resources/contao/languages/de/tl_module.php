<?php

/**
 * @package   Memo\ElasticSearchBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_module']['search_legend'] 						= 'Such-Einstellungen';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['elasticSearchApi']					= array('ElasticSearch API', 'Welche Konfiguration soll für die ElasticSearch API verwendet werden? Im config.yml zu definieren - siehe Dokumentation.');

$GLOBALS['TL_LANG']['tl_module']['autocomplete']						= array('Autocomplete aktivieren', 'Eine Liste mit Vorschlägen von ähnlichen Suchbegriffen einblenden.');
$GLOBALS['TL_LANG']['tl_module']['fuzzy']								= array('Ungenaue Suche', 'Das Keyword muss nicht 100% mit diesem im Resultat übereinstimmen.');
$GLOBALS['TL_LANG']['tl_module']['fuzziness']							= array('Wie ungenau soll die Suche sein?', 'Geben Sie eine Zahl/Auto ein. 0 bis 2 = muss genau stimmen | 3 bis 5 = eine Abweichung erlaubt | > 5 = zwei Abweichungen erlaubt');
$GLOBALS['TL_LANG']['tl_module']['pagination']							= array('Pagination aktivieren', 'Weitere Suchresultate mit einer Pagination anzeigen');
$GLOBALS['TL_LANG']['tl_module']['limit']								= array('Anzahl Resultate pro Seite', 'Wie viele Einträge sollen pro Seite angezeigt werden? (ohne Pagination gibt es nur eine Seite)');
$GLOBALS['TL_LANG']['tl_module']['redirect']							= array('Weiterleitungsseite', 'Wohin soll nach Absenden des Formulars weitergeleitet werden?');
$GLOBALS['TL_LANG']['tl_module']['highlight_result']					= array('Treffer markieren', 'Treffer im Resultat mit einem Tag markieren');
$GLOBALS['TL_LANG']['tl_module']['highlight_fields']					= array('Betreffende Felder', 'Welche Felder sollten markiert werden?');
$GLOBALS['TL_LANG']['tl_module']['limit_chars']							= array('Anzahl auszugebende Zeichen im Teaser', 'Wie viele Zeichen sollen im Teaser der Resultate ausgegeben werden?');
$GLOBALS['TL_LANG']['tl_module']['no_default_styles']					= array('Default Styles nicht laden', 'Std. Styles werden nicht geladen. (eigenes Styling nötig)');
$GLOBALS['TL_LANG']['tl_module']['no_default_scripts']					= array('Default Scripts nicht laden', 'Standard JQuery Script wird nicht geladen. (eigene JS-Funktionen nötig)');
$GLOBALS['TL_LANG']['tl_module']['searchFields']						= array('Suchfelder Definition', 'In welchen Feldern soll gesucht werden?');
$GLOBALS['TL_LANG']['tl_module']['elasticSearchOperator']				= array('Such-Verhalten', 'Und: Suchergebnisse beinhalten jeden einzelnen eingegebenen Suchbegriff. Oder: Suchergebnisse beinhalten einen der eingegebenen Suchbegriffe');


$GLOBALS['TL_LANG']['tl_module']['searchResultTemplate']				= array('Template Suchesultat', 'Hier können Sie das Template für die Suchresultate auswählen.');
$GLOBALS['TL_LANG']['tl_module']['searchFormTemplate']					= array('Template Suchformular', 'Hier können Sie das Template für das Suchformular auswählen.');
$GLOBALS['TL_LANG']['tl_module']['paginationTemplate']					= array('Template Pagination', 'Hier können Sie das Template für die Pagination auswählen.');


