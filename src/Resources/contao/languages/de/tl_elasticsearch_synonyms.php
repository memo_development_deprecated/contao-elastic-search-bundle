<?php

/**
 * @package   Memo\ElasticSearchBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Legend
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['value']				= array('Synonyme', 'Welche Wörter sind gleichbedeutend (kommaseparierte Liste)?');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['elasticSearchApi']	= array('API Konfiguration', 'Welche Konfiguration soll verknüpft werden?');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['synonym_legend']			= "Synonyme erfassen";
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['new']			= array('Neues Synonym', 'Neues Synonym anlegen');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['show']		= array('Details', 'Infos zum Synonym mit der ID %s');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['edit']		= array('Synonym bearbeiten ', 'Synonym bearbeiten');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['cut']			= array('Synonym Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['copy']		= array('Synonym Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_elasticsearch_synonyms']['delete']		= array('Synonym Löschen ', 'ID %s löschen');
