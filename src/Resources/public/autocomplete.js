jQuery(function($){

	let keyupTimeout;

	$(document).ready(function(){

		$('[elasticsearch]').after("<ul class='suggestion'></ul>");

		$('[elasticsearch]').keyup( function(){

			var $inputField = $(this);

			clearTimeout(keyupTimeout);
			keyupTimeout = setTimeout(function() {

				loadAjax($inputField);

			}, 200);
		})
	});

	function loadAjax($inputField) {

		var $inputField;

		$.ajax({
			type: "POST",
			url:  "/ajax_elasticsearch",
			data: {
				api: $inputField.attr("elasticsearch"),
				keyword: $inputField.val(),
			},
			success: function(result)
			{
				if($(result).length) {
					var strSuggestion = "";

					for(var i in result){
						strSuggestion += "<li class='suggestion__item'>"+result[i].text+"</li>";
					};

					$inputField.siblings(".suggestion").html(strSuggestion).fadeIn();

					$(".suggestion__item").on("click", function(){
						$inputField.val($(this).html());
						$inputField.parents("form").submit();
					});
				}
				else {
					$inputField.siblings(".suggestion").html("").hide();
				}
			},
			error: function(response)
			{
				console.log(response.responseText);
			},
		});
	}


});
