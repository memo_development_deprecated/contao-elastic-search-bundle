<?php

namespace Memo\ElasticSearchBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder('memo_elastic_search');

		$treeBuilder->getRootNode()
			->children()
				->arrayNode('connections')
					->requiresAtLeastOneElement()
					->prototype('array')
					->children()
						->scalarNode('url_match')->end()
						->scalarNode('scheme')->end()
						->scalarNode('host')->end()
						->integerNode('port')->end()
						->scalarNode('port')->end()
						->scalarNode('user')->end()
						->scalarNode('password')->end()
						->scalarNode('search_result_tags')->end()
						->scalarNode('priorities')->end()
						->scalarNode('index')->end()
						->scalarNode('default_language')->end()
						->scalarNode('label')->end()
			->end()
				->end()
			->end();

		return $treeBuilder;
	}
}
