<?php declare(strict_types=1);

/**
 * @package   Memo\MemoElasticSearchBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\ElasticSearchBundle\ContaoManager;

use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouteCollection;
use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Contao\CalendarBundle\ContaoCalendarBundle;
use Contao\NewsBundle\ContaoNewsBundle;
use Memo\ElasticSearchBundle\MemoElasticSearchBundle;
use ElasticSearch;

class Plugin implements BundlePluginInterface, RoutingPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(MemoElasticSearchBundle::class)
				->setLoadAfter([
					ContaoCoreBundle::class,
					ElasticSearch::class
				])
				->setReplace(['elasticSearch']),
        ];
    }
    
     /**
    * Returns a collection of routes for this bundle.
    *
    * @param LoaderResolverInterface $resolver
    * @param KernelInterface         $kernel
    *
    * @return null|RouteCollection
    */
    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel)
    {
        return $resolver
            ->resolve(__DIR__ . '/../Resources/config/routing.yml')
            ->load(__DIR__ . '/../Resources/config/routing.yml');
    }
}
