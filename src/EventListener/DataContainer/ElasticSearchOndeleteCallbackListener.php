<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author	Damian Huwiler, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */
 
namespace Memo\ElasticSearchBundle\EventListener\DataContainer;

use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\DataContainer;
use Memo\ElasticSearchBundle\Model\ElasticSearchIntegrationModel;

class ElasticSearchOndeleteCallbackListener
{
    /**
     * @Callback(table="tl_elasticsearch_integration", target="config.ondelete")
     */
    public function onDeleteCallback(DataContainer $dc): void
    {
		if(ElasticSearchIntegrationModel::checkIndex($dc->id)){
			\Message::addInfo('Index '.$dc->activeRecord->index.' wurde gelöscht.');
		}
		
		ElasticSearchIntegrationModel::deleteIndex($dc->id);
    }
}