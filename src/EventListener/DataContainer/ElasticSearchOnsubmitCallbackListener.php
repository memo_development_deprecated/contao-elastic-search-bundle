<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author	Damian Huwiler, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */
 
namespace Memo\ElasticSearchBundle\EventListener\DataContainer;

use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\DataContainer;
use Memo\ElasticSearchBundle\Model\ElasticSearchIntegrationModel;

class ElasticSearchOnsubmitCallbackListener
{
    /**
     * @Callback(table="tl_elasticsearch_integration", target="config.onsubmit")
     */
    public function onSubmitCallback(DataContainer $dc): void
    {
	    $objElasticSearchApi = ElasticSearchIntegrationModel::findById($dc->id);
		if($objElasticSearchApi->host && $objElasticSearchApi->port && $objElasticSearchApi->user && $objElasticSearchApi->password) {
		
			$client = ElasticSearchIntegrationModel::getClient($dc->id);
		}
    }
}