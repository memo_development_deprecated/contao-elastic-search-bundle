<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   ElasticSearchBundle
 * @author	Damian Huwiler, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */
 
namespace Memo\ElasticSearchBundle\EventListener\DataContainer;

use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\DataContainer;
use Memo\ElasticSearchBundle\Model\ElasticSearchIntegrationModel;

class ElasticSearchOnloadCallbackListener
{
    /**
     * @Callback(table="tl_elasticsearch_integration", target="config.onload")
     */
    public function onLoadCallback(DataContainer $dc): void
    {
	    if($dc->id) {
		    $objElasticSearchApi = ElasticSearchIntegrationModel::findById($dc->id);
		    if($objElasticSearchApi->host && $objElasticSearchApi->port && $objElasticSearchApi->user && $objElasticSearchApi->password && \Input::get('act') == 'edit'){
			    $client = ElasticSearchIntegrationModel::getClient($dc->id);
			    
			    if(ElasticSearchIntegrationModel::checkApi($client)){
					\Message::addConfirmation('Verbindung zum Server '.$objElasticSearchApi->host.':'.$objElasticSearchApi->port.' konnte hergestellt werden.');
				}
				else {
					\Message::addError('Verbindung zum Server '.$objElasticSearchApi->host.':'.$objElasticSearchApi->port.' konnte nicht hergestellt werden.');
				}
				
				if(!ElasticSearchIntegrationModel::checkIndex($dc->id)){
					\Message::addInfo('Index '.$objElasticSearchApi->index.' wurde noch nicht angelegt.');
				}
			}
		}
    }
}