<?php

namespace Memo\ElasticSearchBundle\Controller;
use Memo\ElasticSearchBundle\Service\ElasticSearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class AjaxController extends Controller
{
    /**
     * @Route("/ajax_elasticsearch")
     */
    public function searchKeywordAction(Request $request)
    {
	    $strKeyword = $request->request->get('keyword');
	    $strElasticSearchApiId = $request->request->get('api');

	    //Get ElasticSearch Config
		$objService = ElasticSearchService::getServiceByIndex($strElasticSearchApiId);
	    $arrSearchFields = $objService->getSearchFields();

	    if($strKeyword && $strElasticSearchApiId) {

		    $arrSearchResult = $objService->getAutocomplete($strKeyword, $arrSearchFields);
			$arrAllSuggestions = array();

		    foreach($arrSearchResult['suggest'] as $arrSuggestionLevel1) {
			    foreach($arrSuggestionLevel1 as $arrSuggestionLevel2) {
				    foreach($arrSuggestionLevel2['options'] as $arrSuggestionLevel3) {
					    if(!$arrAllSuggestions[$arrSuggestionLevel3['text']]){
					    	$arrAllSuggestions[$arrSuggestionLevel3['text']] = $arrSuggestionLevel3;
						}
						elseif($arrAllSuggestions[$arrSuggestionLevel3['text']]['score'] < $arrSuggestionLevel3['score']) {
					    	$arrAllSuggestions[$arrSuggestionLevel3['text']] = $arrSuggestionLevel3;
						}
				    }
				}
		    }
	    }

	    return new JsonResponse($arrAllSuggestions);
    }

}
