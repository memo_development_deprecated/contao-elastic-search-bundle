<?php
/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   BaseBundle
 * @author	Damian Huwiler, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\ElasticSearchBundle\Module;

use Contao\StringUtil;
use Memo\ElasticSearchBundle\Model\ElasticSearchFieldsModel;
use Memo\ElasticSearchBundle\Model\ElasticSearchFieldsRelationModel;
use Memo\ElasticSearchBundle\Model\ElasticSearchSynonymsModel;
use Memo\ElasticSearchBundle\Service\ElasticSearchService;


class ElasticSearchFormModule extends \Module
{
	/**
	* Template
	* @var string
	*/

	protected $strTemplate = 'elasticsearch_wrapper';

	protected function compile()
	{
		global $objPage;

		$strKeyword = \Contao\Input::post('search');
        $strKeyword = empty($strKeyword)? \Contao\Input::get('search') : $strKeyword;
		$intPage = \Contao\Input::get('page');
		$objService = ElasticSearchService::getServiceByIndex($this->elasticSearchApi);
		$arrConfig  = ElasticSearchService::getConfigByIndex($this->elasticSearchApi);

        if($objService == false) {
            throw new \Exception('No search index selected');
        }

		//Set default Parameters
		$arrParams = [
			'index' => $arrConfig['index'],
			'body'  => [
				'query' => [
					'bool' => [
                        'must' => [
                            'multi_match' => [
                                'query' => $strKeyword,
                                'type' => 'best_fields',
                                'operator' => 'and'
                            ]
                        ]
                    ]
				]
			]
		];

		//if limit
		if($this->limit != 0) {
			$arrParams['body']['size'] = $this->limit;
		}

		//If synonyms
		$objSynonyms = ElasticSearchSynonymsModel::findOneBy('elasticSearchApi',$arrConfig['index']);
		if($objSynonyms) {
			$arrParams['body']['query']['bool']['must']['multi_match']['analyzer'] = 'my_synonyms';
		}

		//Configure fuzzy
		if($this->fuzzy) {
			$arrParams['body']['query']['bool']['must']['multi_match']['fuzziness'] = $this->fuzziness;
			$arrParams['body']['query']['bool']['must']['multi_match']['type'] = 'best_fields';
		}

		//Multiple fields | get Field Config
		$oSearchFields = ElasticSearchFieldsRelationModel::findBy('pid',$this->searchFields);
		if(!empty($oSearchFields))
		{
			foreach ($oSearchFields as $arrSearchField) {
				$arrParams['body']['query']['bool']['must']['multi_match']['fields'][] = $arrSearchField->search_fields . "^" . $arrSearchField->weight;
			}
		}

		//If Page is set in URL
		if($intPage) {
			$arrParams['body']['from'] = $intPage * $this->limit;
		}

		$arrSearchFields = array();
		if(!empty($oSearchFields)) {
			foreach($oSearchFields as $key => $val) {
				$arrSearchFields[] = $val->search_fields;
			}
		}

        if(!empty($this->elasticSearchOperator)) {
            $arrParams['body']['query']['bool']['must']['multi_match']['operator'] = $this->elasticSearchOperator;
        }

		if($strKeyword) {
			$arrSearchResult = $objService->getSearchResultByCustomParam($arrParams);

			if($arrSearchResult){
				$this->Template->searchResult = $this->parseItems($arrSearchResult['hits']['hits']);
			}

			$arrKeywordSuggestion = $objService->getSearchSuggestion($strKeyword, $arrSearchFields);

			$arrFinalSuggestions = array();
			if($arrKeywordSuggestion['suggest']) {
				foreach ($arrKeywordSuggestion['suggest'] as $arrSuggestionLevel1) {
					foreach ($arrSuggestionLevel1 as $arrSuggestionLevel2) {
						foreach ($arrSuggestionLevel2['options'] as $arrSuggestionLevel3) {
							if ($arrFinalSuggestions['score'] < $arrSuggestionLevel3['score']) {
								$arrFinalSuggestions = $arrSuggestionLevel3;
							}
						}
					}
				}
			}
		    if($arrFinalSuggestions) {
			    $arrFinalSuggestions['link'] = $this->replaceInsertTags('{{link_url::'.$objPage->id.'}}')."?search=".$arrFinalSuggestions['text'];
			    $this->Template->mainSuggestion = $arrFinalSuggestions;
		    }
		}

		$objFormTemplate = new \FrontendTemplate($this->searchFormTemplate);

		if($this->autocomplete && $arrSearchFields) {

			$objFormTemplate->elasticSearchApi = $this->elasticSearchApi;
			$objFormTemplate->searchFields = json_encode($arrSearchFields);
		}

		if($this->redirect) {
			$objFormTemplate->redirect = "{{link_url::".$this->redirect."}}";
		}
		else {
			$objFormTemplate->redirect = "{{env::request}}";
		}
        $objFormTemplate->strKeyword = $strKeyword;
		$this->Template->searchForm = $objFormTemplate->parse();

		// Get custom Template
		if( $this->customTpl )
		{
			$this->Template->strTemplate = $this->customTpl;
		}

		//Set Total Var
		$this->Template->total = $arrSearchResult['hits']['total']['value'];

		// Parse Template
		$this->Template->parse();

		//Check if autocomplete is activated
		if($this->autocomplete) {

			//Load JavaScript-File
			if($this->no_default_scripts != 1) {
				$GLOBALS['TL_JAVASCRIPT'][] = "bundles/memoelasticsearch/autocomplete.js";
			}
		}

		if($this->no_default_styles != 1) {
			$GLOBALS['TL_CSS'][] = "bundles/memoelasticsearch/elastic-search-base.css|static";
		}

		//If Pagination is activated

		if($this->pagination && $arrSearchResult && $arrSearchResult['hits']['total']['value'] != 0 && $this->limit != 0) {
			$intCountPages = $arrSearchResult['hits']['total']['value'] / $this->limit;
			$intCountPages = ceil($intCountPages);

			$objPaginationTemplate = new \FrontendTemplate($this->paginationTemplate);
			$objPaginationTemplate->intPages = $intCountPages;
			$objPaginationTemplate->intActivePage = $intPage;
			$objPaginationTemplate->strKeyword = $strKeyword;
			$objPaginationTemplate->strClass = $this->paginationTemplate;
			$this->Template->pagination = $objPaginationTemplate->parse();
		}
		else {
			$this->Template->pagination = "";
			$this->Template->strKeyword = $strKeyword;
		}
	}

    /**
     * @param $arrItems
     * @return string
     */
    public function parseItems($arrItems) {
		foreach($arrItems as $arrItem) {
			$arrParsedItems .= $this->parseItem($arrItem);
		}

		return $arrParsedItems;
	}

    /**
     * @param $arrItem
     * @return string
     */
	public function parseItem($arrItem) {
		$objTemplate = new \FrontendTemplate($this->searchResultTemplate);
		$objTemplate->setData($arrItem);
		$objTemplate->limit_chars = $this->limit_chars;
		$objTemplate->strClass = $this->searchResultTemplate;

		return $objTemplate->parse();
	}
}
