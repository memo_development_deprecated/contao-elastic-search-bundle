<?php declare(strict_types=1);

/**
 * @package   Memo\MemoElasticSearchBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\ElasticSearchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoElasticSearchBundle extends Bundle
{
}
