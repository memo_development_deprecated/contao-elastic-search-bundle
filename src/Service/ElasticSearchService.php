<?php

namespace Memo\ElasticSearchBundle\Service;

use Elasticsearch\ClientBuilder;
use Memo\ElasticSearchBundle\Model\ElasticSearchSynonymsModel;
use Symfony\Component\Config\Definition\Exception\Exception;

class ElasticSearchService
{

	var $client;
	var $config;

	/**
	 * ElasticSearchService constructor.
	 * @param array $config
	 */
	public function __construct(array $config)
	{
		if (empty($config))
		{
			\System::log("Elastic Search Obj can't create. empty config",__METHOD__,TL_ERROR);
			return false;
		}
		$this->config = $config;
		try {
			$this->getClient();
			if (!$this->checkApi()) {
				throw new Exception("Wrong Elastic Search API Config");
			}
		}catch (\Throwable $e) {
			\System::log("Elastic Search API not callable", __METHOD__, TL_ERROR);
			return false;
		}

	}

	/**
	 * @return array|false
	 */
	public function createIndex()
	{

        $params = [
            "body" => [
                "settings" => [
                    "index" => [
                        "analysis" => [
                            "filter" => [
                                "stemmer" => [
                                    "type" => "stemmer",
                                    "language" => "german"
                                ],
                                "stopwords" => [
                                    "type" => "stop",
                                    "stopwords" => "_german_"
                                ]
                            ],
                            "analyzer" => [
                                "autocomplete" => [
                                    "filter" => ["lowercase","stopwords"],
                                    "tokenizer" => "standard",
                                    "char_filter" => ["memo_html_filter"],
                                    "type" => "custom"
                                ],
                                "default" => [
                                    "filter" => ["lowercase", "stopwords","shingle"],
                                    "char_filter" => ["memo_html_filter"],
                                    "type" => "custom",
                                    "tokenizer" => "standard"
                                ],
                                "withHtml" => [
                                    "filter" => ["lowercase", "stopwords","shingle"],
                                    "type" => "custom",
                                    "tokenizer" => "standard"
                                ]
                            ],
                            "char_filter" => [
                                "memo_html_filter" => [
                                    "type" => "html_strip",
                                    "escaped_tags" => []
                                ]
                              ]
                        ]
                    ]
                ],
                "mappings" => [
                    //"_default_" => [
                    "properties" => [
                        "h1" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "h2" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "h3" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "h4" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "h5" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "h6" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "header_content_text" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "header_content_html" => [
                            "type" => "text",
                            "analyzer" => "withHtml",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "main_content_text" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "main_content_html" => [
                            "type" => "text",
                            "analyzer" => "withHtml",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "footer_content_html" => [
                            "type" => "text",
                            "analyzer" => "withHtml",
                            "search_analyzer" => "standard"
                        ],
                        "footer_content_text" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "pdf_content" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "title" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "alias" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "meta_description" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard",
                            "copy_to" => [
                                "suggest"
                            ]
                        ],
                        "meta_image_alt" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "memoUuid" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "language" => [
                            "type" => "text",
                            "analyzer" => "default",
                            "search_analyzer" => "standard"
                        ],
                        "suggest" => [
                            "type" => "completion",
                            "analyzer" => "autocomplete",
                            "copy_to" => [
                                "suggest"
                            ]
                        ]
                    ]
                    //]
                ]
            ]
        ];


		$client = $this->getClient();
		if($this->checkApi()){
			$objSynonyms = ElasticSearchSynonymsModel::findAll();
			$params['index'] = $this->config['index'];

			if(isset($objSynonyms)){
				$params['body']['settings']['index']['analysis']['analyzer']['my_synonyms']['tokenizer'] = 'standard';
				$params['body']['settings']['index']['analysis']['analyzer']['my_synonyms']['filter'][] = "lowercase";
				$params['body']['settings']['index']['analysis']['analyzer']['my_synonyms']['filter'][] = "my_synonym_filter";
				$params['body']['settings']['index']['analysis']['filter']["my_synonym_filter"]['type'] = "synonym";

				foreach($objSynonyms as $objSynonym) {
					$params['body']['settings']['index']['analysis']['filter']['my_synonym_filter']['synonyms'][] = $objSynonym->value;
				}
			}
			$response = $this->client->indices()->create($params);
			\System::log("Elastic Search Index:".$this->config['index']." successfully created.",__METHOD__,TL_GENERAL);
		}
		else {
			$response = false;
			\System::log("C'ant create elastic Search Index:".$this->config['index'].". Check Config",__METHOD__,TL_ERROR);
		}
		return $response;
	}

	/**
	 * @return false
	 */
	public function deleteIndex() {

		if($this->checkIndex()){
			$params = ['index' => $this->config['index']];
			$response = $this->client->indices()->delete($params);
		}else {
			$response = false;
		}
		return $response;
	}

	/**
	 * @return \Elasticsearch\Client|false
	 */
	public function getClient()
	{
		try {
			if (isset($this->config['user']) && isset($this->config['password']) && isset($this->config['host']) && isset($this->config['port'])) {
				$this->client = ClientBuilder::create()
					->setBasicAuthentication($this->config['user'], $this->config['password'])
					->setHosts([
						$this->config['scheme'] . "://" . $this->config['host'] . ":" . $this->config['port']
					])
					->build();
			} else {
				Throw new Exception("Elastic Search Client cant connect");
			}

			$this->client->Scheme = $this->config['scheme'];
			$this->client->Host = $this->config['host'];
			$this->client->Port = $this->config['port'];

		}catch(\Throwable $x) {
			\System::log("Elastic Search Client can't connect. check config",__METHOD__,TL_ERROR);
			return false;
		}
		return $this->client;
	}


	/**
	 * @param $client
	 * @return false|mixed
	 */
	public function checkApi() {
		try {
			$health = $this->client->cluster()->health();
		} catch(\Throwable $x) {
			return false;
		}
		return $health;
	}

	/**
	 * @return bool
	 */
	public function checkIndex(): bool {
		return $this->client->indices()->exists(['index' => $this->config['index']]);
	}

	/**
	 * @param $intId
	 * @return array
	 */
	public function getSearchFields()
	{
		$arrFields = [];
		$strIndex  = $this->config['index'];

		$params = [
			'index' => $strIndex
		];

		$response = $this->client->indices()->getMapping($params);
		if($response[$strIndex]['mappings']['properties']){
			foreach($response[$strIndex]['mappings']['properties'] as $key => $value) {
				$arrFields[] = $key;
			}
		}

		return $arrFields;
	}


	/**
	 * @param $strIndex
	 * @return false|ElasticSearchService
	 */
	static function getServiceByIndex($strIndex){

		if($arrConfigs = \System::getContainer()->getParameter('memo_elastic_search.connections')){
			foreach($arrConfigs as $arrConfig){
				if($arrConfig['index'] == $strIndex){
					if($objService = new ElasticSearchService($arrConfig)){
						return $objService;
					}
				}
			}
		}

		return false;

	}

	static function getConfigByIndex($strIndex){

		if($arrConfigs = \System::getContainer()->getParameter('memo_elastic_search.connections')){
			foreach($arrConfigs as $arrConfig){
				if($arrConfig['index'] == $strIndex){

					if($arrPriorities = explode(',',  $arrConfig['priorities'])){
						$arrPriorityDetails = array();
						foreach($arrPriorities as $strPriority){
							$arrPriorityDetails[] = explode(':', $strPriority);
						}

						$arrConfig['priorities'] = $arrPriorityDetails;
					}


					return $arrConfig;
				}
			}
		}
		return false;
	}

	/**
	 * @param array $aParams
	 * @return array
	 */
	public function getSearchResultByCustomParam($aParams = array()): array
	{
		$response = [];
		$response = $this->client->search($aParams);
		return $response;
	}

	/**
	 * @param $strKeyword
	 * @param $arrSearchFields
	 * @return array
	 */
	public function getSearchSuggestion($strKeyword=NULL, $arrSearchFields=array()): array
	{
        $response = [];
        if (empty($arrSearchFields))
        {
            return $response;
        }

        $params = [
            'index' => $this->config['index'],
            'body'  => [
                'suggest' => [
                    'text' => $strKeyword
                ]
            ]
        ];

        $aIndexedFields = $this->getSearchFields();
        //Nur wenn Index auf Server Daten enthält
        if(!empty($aIndexedFields)) {

            foreach ($arrSearchFields as $strSearchField) {
                //Prüft ob das Suchfeld im Index vorhanden ist, ansonsten wird dieses ignoriert.
                if(in_array($strSearchField,$aIndexedFields)) {
                    $params['body']['suggest']['suggest-' . $strSearchField] = [
                        'term' => [
                            'field' => $strSearchField,
                            'min_word_length' => 2
                        ],
                    ];
                }
            }
            $response = $this->client->search($params);
        }
        return $response;
	}

    /**
     * @param null $strKeyword
     * @param array $arrSearchFields
     * @return array
     */
    public function getAutocomplete($strKeyword=NULL, $arrSearchFields=array()): array
	{
		$response = [];
		if (empty($arrSearchFields))
		{
			return $response;
		}
        $aIndexedFields = $this->getSearchFields();

		$params = [
			'index' => $this->config['index'],
			'body'  => [
				'suggest' => [
                    "text" => $strKeyword,
                    "simple_phrase" => [
                        "completion" => [
                            "field" =>"suggest"
                        ]
                    ]
                ]
			]
		];

        //Nur wenn Index auf Server Daten enthält
        if(!empty($aIndexedFields)) {
            $response = $this->client->search($params);
        }
		return $response;
	}

}
