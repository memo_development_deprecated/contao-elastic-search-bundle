<?php


namespace Memo\ElasticSearchBundle\Search\Indexer;


use Contao\CoreBundle\Search\Document;
use Contao\CoreBundle\Search\Indexer\IndexerException;
use Contao\CoreBundle\Search\Indexer\IndexerInterface;
use Memo\ElasticSearchBundle\Model\ElasticSearchIntegrationModel;
use PhpParser\Node\Expr\Cast\Array_;
use Memo\ElasticSearchBundle\Service\ElasticSearchService;

class MemoSearchIndexer implements IndexerInterface
{

	public function index(Document $document): void
	{
		$aIndexData = [];
		//get Document Url
		$sUrl = sprintf("%s%s", $document->getUri()->getHost(), $document->getUri()->getPath());

		//get Configuration
		$aConfig = $this->getConfiguration($sUrl);
		if(!$aConfig)
		{
			return;
		}

		//ToDo: add structur datas
		$meta = array();
		$this->extendMetaFromJsonLdScripts($document, $meta);

		$oService = new ElasticSearchService($aConfig);

		//Index Page
		$aPageData = $this->parseDocument($document, $aConfig);
		try {
			//Upsert Create or Update Record
			$params = [
				'index' => $aConfig['index'],
				'id' 	=> $aPageData['memoUuid'],
				'body' 	=> [
					'doc' => $aPageData,
					'upsert' => [
						'counter' => 1
					]
				]
			];
			$response = $oService->client->update($params);

		} catch (\Throwable $e) {
			\System::log("Elastic Search PDF index error",__METHOD__,TL_ERROR);
		}

		$aPDFData = $this->parsePDF($document, $aConfig);
		try {
			$params = [];
			foreach ($aPDFData as $key => $aPdf) {
				//Upsert Create or Update Record
				$params = [
					'index' => $aConfig['index'],
					'id' 	=> $aPdf['memoUuid'],
					'body' 	=> [
						'doc' => $aPdf,
						'upsert' => [
							'counter' => 1
						]
					]
				];
				$response = $oService->client->update($params);
			}
		} catch (\Throwable $e) {
			\System::log("Elastic Search PDF index error",__METHOD__,TL_ERROR);
		}
	}

	public function delete(Document $document): void
	{
		// TODO: Implement delete() method.
		\System::log("Delete",__METHOD__,TL_ERROR);
	}

	/**
	 * Delete all Indexes
	 */
	public function clear(): void
	{
		//Create ElasticSearch Index

		//get Configuration
		$aConfigs = $this->getConfiguration();

		foreach($aConfigs as $key => $aConfig)
		{
			$oService = new ElasticSearchService($aConfig);
			$oService->deleteIndex();
			$oService->createIndex();
		}

		\System::log("Elastic Search Reset all Indexes",__METHOD__,TL_GENERAL);
	}


	/**
	 * @param null $prmUrl
	 * @return Array
	 */
	private function getConfiguration($prmUrl = NULL)
	{

		$aConnections = \System::getContainer()->getParameter('memo_elastic_search.connections');

		if(empty($prmUrl)){
			return $aConnections;
		}

		foreach($aConnections as $aConnection)
		{
			if(preg_match($aConnection['url_match'],$prmUrl))
			{
				return $aConnection;
			}
		}

		return false;
	}


	/**
	 * @param Document $document
	 * @param array $meta
	 */
	private function extendMetaFromJsonLdScripts(Document $document, array &$meta): void
	{
		$jsonLds = $document->extractJsonLdScripts('https://schema.contao.org/', 'Page');

		if (0 === \count($jsonLds)) {
			$jsonLds = $document->extractJsonLdScripts('https://schema.contao.org/', 'RegularPage');

			if (0 === \count($jsonLds)) {
				$this->throwBecause('No JSON-LD found.');
			}

			@trigger_error('Using the JSON-LD type "RegularPage" has been deprecated and will no longer work in Contao 5.0. Use "Page" instead.', E_USER_DEPRECATED);
		}

		// Merge all entries to one meta array (the latter overrides the former)
		$meta = array_merge($meta, array_merge(...$jsonLds));
	}


	/**
	 * @param $document
	 * @param $prmConfig
	 * @return array
	 */
	public function parseDocument($document, $prmConfig = array()) {

		if(empty($prmConfig)){
			return [];
		}
		$arrImageTitle 	= [];
		$arrImageAlt	= [];
		$sUrl = $document->getUri()->getPath();
		$sHost = $document->getUri()->getHost();

		//Remove noindex elements
		$sBody = preg_replace('/<\!--\sindexer::stop\s-->([\s\S]*?)<\!--\sindexer::continue\s-->/im', '', $document->getBody());
		$arrParams = [];

		$aSearch = ['/\s+/is','/\r\n|\r|\n/','/(<h[1-6].*?>.*?<\/h[1-6]>)/is','/<script.*?>.*?<\/script>/im','/<style.*?>.*?<\/style>/im', '/\&shy;/im'];
		$aReplace = [' ','[br]','','','',''];

		//Set language
		if(preg_match('/<html.*?lang="(.*?)"[^>]*>/i', $sBody, $aMatch)){
			$arrParams['language'] = $aMatch[1];
		}
		else {
			$arrParams['language'] = empty($prmConfig['default_language'])? 'de' : $prmConfig['default_language'];
		}

		//Replace Headlines, Scripts, Styles and Shy
		$sBodyContent = preg_replace($aSearch, $aReplace, $sBody);




		//Grap body content
		preg_match("/<header[^>]*>(.*)<\/header>/im",$sBodyContent,$aMatchHeader);
		preg_match("/<main[^>]*>(.*)<\/main>/im",$sBodyContent,$aMatchMain);
		preg_match("/<footer[^>]*>(.*)<\/footer>/im",$sBodyContent,$aMatchFooter);

        $htmlHeader = new \Html2Text\Html2Text($aMatchHeader[1]);
        $htmlMain   = new \Html2Text\Html2Text($aMatchMain[1]);
        $htmlFooter = new \Html2Text\Html2Text($aMatchFooter[1]);

		$arrParams['header_content_text'] = $htmlHeader->getText();
		$arrParams['main_content_text']   = $htmlMain->getText();
		$arrParams['footer_content_text'] = $htmlFooter->getText();

		//Replace HTML Tags and strip whitespaces
		$strHeaderContent = strip_tags(trim($aMatchHeader[1]),$prmConfig['search_result_tags']);
		$strMainContent = strip_tags(trim($aMatchMain[1]),$prmConfig['search_result_tags']);
		$strFooterContent = strip_tags(trim($aMatchFooter[1]),$prmConfig['search_result_tags']);

		//Remove Whitespace / empty HTML Tags
		$strHeaderContent 	= preg_replace(['/<[^\/>][^>]*>\s*?<\/[^>]+>/','/\s+/i','/[\r\n|\n|\r]+/'], ['',' ',''], trim($strHeaderContent));
		$strMainContent 	= preg_replace(['/<[^\/>][^>]*>\s*?<\/[^>]+>/','/\s+/i','/[\r\n|\n|\r]+/'], ['',' ',''], trim($strMainContent));
		$strFooterContent 	= preg_replace(['/<[^\/>][^>]*>\s*?<\/[^>]+>/','/\s+/i','/[\r\n|\n|\r]+/'], ['',' ',''], trim($strFooterContent));

		$arrParams['header_content_html'] 	= $strHeaderContent;
		$arrParams['main_content_html'] 	= $strMainContent;
		$arrParams['footer_content_html'] 	= $strFooterContent;

		preg_match_all('/<img(.*?)>/is', $sBody, $arrImgs);

		foreach($arrImgs[1] as $strImg) {
			$arrValues = [];
			if(preg_match('/title="(.*?)"/is', $strImg, $arrTitle)){
				if(!empty($arrTitle[1])){
					$arrImageTitle[] = $arrTitle[1];
				}
			}
			if(preg_match('/alt="(.*?)"/is', $strImg, $arrAlt)){
				if(!empty($arrAlt[1])){
					$arrImageAlt[] = $arrAlt[1];
				}
			}
		}

		preg_match('/<title>(.*?)<\/title>/is', $sBody, $arrMetaTitle);
		preg_match('/<meta[^>]*name=[\"|\']description[\"|\'][^>]*content=[\"]([^\"]*)[\"][^>]*>/is', $sBody, $arrMetaDescription);

		for($i=1;$i<7;$i++)
		{
			preg_match_all('/<h'.$i.'.*?>(.*?)<\/h'.$i.'>/is', $sBody, $aMatch);
			if($aMatch[1])
			{
				foreach($aMatch[1] as $value) {
					$arrParams['h'.$i][] = $value;
				}
			}else {
				$arrParams['h'.$i][] = '';
			}
		}

		$arrParams['url'] 	= $document->getUri()->__toString();
		$arrParams['memoUuid']  = md5($document->getUri()->__toString());
		$arrParams['alias'] = $sUrl;
		$arrParams['title'] = $arrMetaTitle[1];
		$arrParams['meta_description'] = $arrMetaDescription[1];
		$arrParams['meta_image_title'] = $arrImageTitle;
		$arrParams['meta_image_alt'] = $arrImageAlt;
		$arrParams['type'] = 'web';
		$arrParams['pdf_content'] = '';

		// Enable a hook to be loaded here
		if (isset($GLOBALS['TL_HOOKS']['indexPageToElasticSearch']) && \is_array($GLOBALS['TL_HOOKS']['indexPageToElasticSearch']))
		{
			foreach ($GLOBALS['TL_HOOKS']['indexPageToElasticSearch'] as $callback)
			{
				$cb = \Contao\System::importStatic($callback[0]);
				$arrParams = $cb->{$callback[1]}($arrParams, $document, $prmConfig);
			}
		}

		return $arrParams;
	}

	/**
	 * @param $document
	 * @param $prmConfig
	 * @return array|void
	 * @throws \Exception
	 */
	public function parsePDF($document, $prmConfig = array()) {

		if(empty($prmConfig))
		{
			return [];
		}

		//Set language
		if(preg_match('/<html.*?lang="(.*?)"[^>]*>/i', $document->getBody(), $aMatch)){
			$sLang = $aMatch[1];
		} else {
			$sLang = empty($prmConfig['default_language'])? 'de' : $prmConfig['default_language'];
		}

		$aPDF = [];
		$sBody = preg_replace('/<\!-- indexer::stop -->(.|\n|\r\n|\r)*?<\!-- indexer::continue -->/im', '', $document->getBody());
		preg_match_all('/href="(\S+?\.pdf)".*?[\"|\'][^>]*>(.*?)<\/a>/im',$sBody,$aMatchPDF);

        if(!empty($aMatchPDF[0])) {
            foreach ($aMatchPDF[0] as $key => $strPDFLink) {

                $strPath = preg_replace('/.*?file=/i', '', $aMatchPDF[1][$key]);
                $memoUuid = md5(preg_replace('/.*?file=/i', '', $aMatchPDF[1][$key]));

                //Check external PDF's an skip
                if (substr($strPath, 0, 4) === 'http' && !stristr($strPath, $prmConfig['host'])) {
                    continue;
                }

                $aPDF[$memoUuid]['url'] = $aMatchPDF[1][$key];
                $aPDF[$memoUuid]['path'] = urldecode($strPath);
                $aPDF[$memoUuid]['title'] = strip_tags($aMatchPDF[2][$key]);
                $aPDF[$memoUuid]['type'] = 'pdf';
                $aPDF[$memoUuid]['memoUuid'] = $memoUuid;
                $aPDF[$memoUuid]['src_index_path'] = $document->getUri()->getPath();

                $aPDF[$memoUuid]['meta_title'] = '';
                $aPDF[$memoUuid]['meta_alt'] = '';
                $aPDF[$memoUuid]['meta_caption'] = '';
                $aPDF[$memoUuid]['meta_link'] = '';
                $aPDF[$memoUuid]['name'] = '';

                //Overwrite Content Title with Meta Title
                $objFile = \FilesModel::findByPath($aPDF[$memoUuid]['path']);
                $aMeta = [];
                if ($objFile) {
                    if (!empty($objFile->name)) {
                        $aPDF[$memoUuid]['name'] = $objFile->name;
                    }

                    $aMeta = unserialize($objFile->meta)[$sLang];

                    if (!empty($aMeta['title'])) {
                        $aPDF[$memoUuid]['meta_title'] = $aMeta['title'];
                    }
                    if (!empty($aMeta['alt'])) {
                        $aPDF[$memoUuid]['meta_alt'] = $aMeta['alt'];
                    }
                    if (!empty($aMeta['link'])) {
                        $aPDF[$memoUuid]['meta_link'] = $aMeta['link'];
                    }
                    if (!empty($aMeta['caption'])) {
                        $aPDF[$memoUuid]['meta_caption'] = $aMeta['caption'];
                    }

                }

                try {
                    $documentUrl = sprintf("%s://%s/%s", $document->getUri()->getScheme(), $document->getUri()->getHost(), $aPDF[$memoUuid]['url']);
                    $parser = new \Smalot\PdfParser\Parser();
                    $pdf = $parser->parseFile($documentUrl);
                } catch (\Throwable $e) {
                    unset($aPDF[$memoUuid]); //remove wrong pdf item
                    continue;
                }

                $arrDetails = $pdf->getDetails();
                $aPDF[$memoUuid]['creation_date'] = $arrDetails['CreationDate'];
                $arrPages = $pdf->getPages();
                $strText = "";

                foreach ($arrPages as $page) {
                    $strText .= str_replace(
                        ["\n", "\r", "\t", "\ "],
                        [" ", " ", " ", ' '],
                        $page->getText()
                    );
                    $strText .= "\n";
                }

                $strText = rtrim($strText);

                $aPDF[$memoUuid]['pdf_content'] = $strText;

                // Enable a hook to be loaded here
                if (isset($GLOBALS['TL_HOOKS']['indexPdfToElasticSearch']) && \is_array($GLOBALS['TL_HOOKS']['indexPdfToElasticSearch'])) {
                    foreach ($GLOBALS['TL_HOOKS']['indexPdfToElasticSearch'] as $callback) {
                        $cb = \Contao\System::importStatic($callback[0]);
                        $aPDF[$memoUuid] = $cb->{$callback[1]}($aPDF[$memoUuid], $strPDFLink, $arrDetails, $prmConfig, $document);
                    }
                }
            }
        }
		return($aPDF);
	}


}
